using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class qqqq : MonoBehaviour
{
    bool willDestroy = false;

    public void Destory2(string reson,int sign)
    {
        Debug.LogError($"StartDestory {reson}  \t\tcurFrameCount:{sign} _______________________________________");
        willDestroy = true;
        GameObject.Destroy(this);
    }

    void Update()
    {
        if (willDestroy)
        {
            Debug.LogError($"Update   \t\tcurFrameCount:{Time.frameCount}");
        }
    }
    private void FixedUpdate()
    {
        if (willDestroy)
        {
            Debug.LogError($"FixedUpdate   \t\tcurFrameCount:{Time.frameCount}");
        }
    }
    private void LateUpdate()
    {
        if (willDestroy)
        {
            Debug.LogError($"LateUpdate   \t\tcurFrameCount:{Time.frameCount}");
        }
    }

    private void OnDisable()
    {
        if (willDestroy)
        {
            Debug.LogError($"OnDisable   \t\tcurFrameCount:{Time.frameCount}     ---");
        }
    }
    private void OnDestroy()
    {
        if (willDestroy)
        {
            Debug.LogError($"OnDestroy   \t\tcurFrameCount:{Time.frameCount}     -------");
        }
    }
}
