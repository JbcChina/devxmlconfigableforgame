using System;
using System.Collections.Generic;
using System.Reflection;
using System.Xml.Serialization;
using UnityEditor;
using UnityEngine;
using XmlConfigable;

public class TestWindow : EditorWindow
{
    [MenuItem("Test/TestWindow")]
    public static void OpenTestWindow()
    {
        EditorWindow.GetWindow<TestWindow>("Configable2Debug");
    }

    public readonly static GUILayoutOption height_15 = GUILayout.Height(15);
    public readonly static GUILayoutOption height_20 = GUILayout.Height(20);
    public readonly static GUILayoutOption height_25 = GUILayout.Height(25);
    public readonly static GUILayoutOption height_22 = GUILayout.Height(22);
    public readonly static GUILayoutOption height_30 = GUILayout.Height(30);
    public readonly static GUILayoutOption height_40 = GUILayout.Height(40);
    public readonly static GUILayoutOption height_400 = GUILayout.Height(400);
    public readonly static GUILayoutOption height_500 = GUILayout.Height(500);
    public readonly static GUILayoutOption height_600 = GUILayout.Height(600);
    public readonly static GUILayoutOption height_300 = GUILayout.Height(300);
    public readonly static GUILayoutOption height_200 = GUILayout.Height(200);
    public readonly static GUILayoutOption height_180 = GUILayout.Height(180);
    public readonly static GUILayoutOption height_800 = GUILayout.Height(800);
    public readonly static GUILayoutOption height_80 = GUILayout.Height(80);
    public readonly static GUILayoutOption width_70 = GUILayout.Width(70);
    public readonly static GUILayoutOption width_90 = GUILayout.Width(90);
    public readonly static GUILayoutOption width_50 = GUILayout.Width(50);
    public readonly static GUILayoutOption width_60 = GUILayout.Width(60);
    public readonly static GUILayoutOption width_40 = GUILayout.Width(40);
    public readonly static GUILayoutOption width_20 = GUILayout.Width(20);
    public readonly static GUILayoutOption width_30 = GUILayout.Width(30);
    public readonly static GUILayoutOption width_25 = GUILayout.Width(25);
    public readonly static GUILayoutOption width_300 = GUILayout.Width(300);
    public readonly static GUILayoutOption width_330 = GUILayout.Width(330);
    public readonly static GUILayoutOption width_600 = GUILayout.Width(600);
    public readonly static GUILayoutOption width_100 = GUILayout.Width(100);
    public readonly static GUILayoutOption width_120 = GUILayout.Width(120);
    public readonly static GUILayoutOption width_150 = GUILayout.Width(150);
    public readonly static GUILayoutOption width_130 = GUILayout.Width(130);
    public readonly static GUILayoutOption width_170 = GUILayout.Width(170);
    public readonly static GUILayoutOption width_200 = GUILayout.Width(200);
    public readonly static GUILayoutOption width_400 = GUILayout.Width(400);

    private void OnGUI()
    {
        RefreshDebugConfig();
    }

    bool configPageInit = false;
    GUIContent 生成DebugConfig文件 = new GUIContent("生成DebugConfig文件");
    GUIContent 更新xml新增配置文件 = new GUIContent("更新xml新增配置文件");
    GUIContent 防止生成DebugConfig文件误触锁 = new GUIContent("lock", "防止生成DebugConfig文件误触");
    bool rewriteLock = true;

    void InitDebugConfig()
    {
        if (!configPageInit)
        {
            configPageInit = true;
        }
    }
    void RefreshDebugConfig()
    {
        InitDebugConfig();

        GUILayout.BeginHorizontal();
        if (GUILayout.Button("读取本地数据", width_100))
        {
            XmlConfigurableSys.Instance.LoadXmlConfig();
            Debug.LogError("读取完成。");
        }

        if (GUILayout.Button("删除本地数据", width_100))
        {
            if (rewriteLock)
            {
                Debug.LogError("打开后才能生成 —— " + 防止生成DebugConfig文件误触锁.text);
                return;
            }
            DeleteConfigFile();
            configPageInit = false;
            AssetDatabase.SaveAssets();
            AssetDatabase.Refresh();
        }
        if (GUILayout.Button("打开xml目录", width_100))
        {

            TestFunc();

        }

        GUILayout.EndHorizontal();


        GUILayout.BeginHorizontal();
        if (GUILayout.Button(生成DebugConfig文件, width_150))
        {
            if (rewriteLock)
            {
                Debug.LogError("打开后才能生成 —— " + 防止生成DebugConfig文件误触锁.text);
                return;
            }
            //XMLSysCollecter.CollecterXMLSysData(new AddDevXMLSysAttribute(typeof(object)));
            CreateConfigFile();
        }

        if (GUILayout.Button(更新xml新增配置文件, width_150))
        {
            var ss = typeof(DevConfigBuildingGMData).GetCustomAttributes(false);
            Debug.LogError(ss);
        }

        rewriteLock = GUILayout.Toggle(rewriteLock, 防止生成DebugConfig文件误触锁, width_70);
        GUILayout.EndHorizontal();

        RefreshConfigElements();




        GUILayout.Label("------------------------------------------------------------");
        if (GUILayout.Button("新的导出", width_150))
        {
            if (rewriteLock)
            {
                Debug.LogError("打开后才能生成 —— " + 防止生成DebugConfig文件误触锁.text);
                return;
            }
            XmlConfigurableSys.Instance.ExportXmlData();
            Debug.LogError("导出完成。");
        }
        if (GUILayout.Button("新的读取", width_150))
        {
            //XmlConfigurableSys.Instance.LoadXmlConfig();
            Debug.LogError("读取完成。");
        }



    }

    [Serializable]
    public class Test
    {
        [XmlElement("intLstItem")]
        public List<int> intLst = new() { 1,2};
    }

    private void TestFunc()
    {
        XmlConfigurableSys.Instance.LoadXmlConfig();
        Debug.LogError("读取完成。");
        //ExportAndLoadProxy export = new(Application.dataPath, "JJ1");
        //var sss = new DDDD();
        //export.Export(sss);
        //var xmlData = export.LoadXml(typeof(DDDD));
        //Debug.LogError(xmlData);
        //var typeName1 = Type.GetType("XmlConfigable.XmlDefine.Common_ModuleType");
        //var typeName2 = Type.GetType("XmlDefine.Common_ModuleType");
        //var typeName3 = Type.GetType("XmlConfigable.XmlDefine+Common_ModuleType");
        //Debug.LogError($"1:{typeName1}   2:{Convert.ToInt32( Enum.Parse(typeName3, "T_SpeicalSwitch"))}    3:{typeName3}");
        //Debug.LogError(typeof(XmlDefine.Common_ModuleType));
        //Debug.LogError(Enum.Parse(typeName1, "T_SpeicalSwitch"));
        //var aV = new AAAAAAAA();

        //var curType = typeof(AAAAAAAA);

        //var fields = curType.GetFields();

        //foreach (var field in fields)
        //{
        //    var value = field.GetValue(aV);
        //    if (value as AAAA != null)
        //    {
        //        Debug.LogError((value as AAAA).aaV.a);
        //    }
        //}
        //var assembly = Assembly.GetAssembly(typeof(bb));
        //XmlLoadPriority at = Attribute.GetCustomAttribute(typeof(bb), typeof(XmlLoadPriority)) as XmlLoadPriority;
        //Debug.LogError(at.loadPriority);


        //PrintData printData = new() { configId = 10, moduleName = "dd", curTrusteeshipLst = new() { new PrintTrusteeshipPackage() { moduleName = "packageName", configId = 15 } } };
        //PrintData printData2 = new() { configId = 122222, moduleName = "dd2222"};
        //ExportAndLoadProxy ioProxy = new("K:/", "PrintData");

        //List<Type> Types = new() { typeof(PrintElement),typeof(PrintTrusteeshipPackage) };

        ////ioProxy.Export(new PrintTrusteeshipPackage());
        ////ioProxy.Export(printData, Types.ToArray());
        ////printData.curTrusteeshipLst2 = printData.curTrusteeshipLst.ToArray();
        //ioProxy.Export(printData, Types.ToArray());
        //ioProxy.Export(new Test());


        AutomatedGeneration.GenerationInstanceTypeMapping();
    }

    void RefreshConfigElements()
    {

        //if (TrusteeVoucherChecker.Instance.GetAllVoucherHolder() != null)
        //{
        //    foreach (DevXMLConfigBase_AddName item in TrusteeVoucherChecker.Instance.GetAllVoucherHolder())
        //    {
        //        item.EditorWindowRefresh();
        //    }
        //}

    }

    void DeleteConfigFile()
    {
        //var allRootHandlers = DevDefine.GetAllXmlRootSys();
        //foreach (var rootHandler in allRootHandlers)
        //{
        //    rootHandler.DeleLocalConfigFile();
        //}
    }
    void CreateConfigFile()
    {
        //var allRootHandlers = DevDefine.GetAllXmlRootSys();
        //foreach (var rootHandler in allRootHandlers)
        //{
        //    rootHandler.OnPreCreateXmlConfigFile2DefaultPath();
        //}
        //foreach (var rootHandler in allRootHandlers)
        //{
        //    rootHandler.CreateXmlConfigFile2DefaultPath();
        //}
        //foreach (var rootHandler in allRootHandlers)
        //{
        //    rootHandler.OnEndCreateXmlConfigFile2DefaultPath();
        //}
    }


}