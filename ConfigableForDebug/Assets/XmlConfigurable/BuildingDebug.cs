using System;
using System.Collections.Generic;
using static XmlConfigable.XmlDefine;

namespace XmlConfigable
{
    public class DevConfigBuildingRootData : RootConfigBase
    {

    }

    [XmlHandlerRootAttribure(XmlRootType.Building, XmlLoadPriority.priorityMidddle, "BuildingRoot模块")]
    public class DevHandlerBuildingRoot : RootHandlerBase<DevHandlerBuildingRoot, DevConfigBuildingRootData>
    {
        public override IXmlProxyExportAndLoad GetXmlIOProxy()
        {
            return new ExportAndLoadProxy(XmlConfigurableUtils.GetDirectryPath(xmlRootType), "ZZ_BuildingRoot");
        }
    }

    public enum BearingLog
    {
        FurnitureInfluenceResult,
        InfluenceCount,
    }

    public class DevConfigBuildingBearingData : ModuleInRootConfigBase
    {

    }

    [XmlHandlerModuleInRoot(XmlRootType.Building, "BuildingBearing")]
    public class DevHandlerInRootBuildingBearing : ModuleInRootHandlerBase<DevHandlerInRootBuildingBearing, DevConfigBuildingBearingData>
    {
        [XmlIndependentTrusteePackageSet(XmlRootType.Building, "BearingLog")]
        public NeedTrusteeshipModuleInRootConfig bearingDev = new()
        {
            TrusteeshiPackageList = new()
            {
                PrintTrusteeshipPackage.GetTrusteeshipPackage<PrintTrusteeshipPackage>
                (   
                    Building_ModuleType.BuildingBearing, 
                    BearingLog.FurnitureInfluenceResult, 
                    BearingLog.InfluenceCount
                ),
            }
        };
    }

    [XmlLoadTrusteePackageModule((int)Building_ModuleType.T_BearingLog)]
    public class DevConfigBuildingGMData : ModuleInRootConfigBase
    {
    }

    [XmlHandlerModuleInRoot(XmlRootType.Building, "GM")]
    public class DevHandlerInRootBuildingGM : ModuleInRootHandlerBase<DevHandlerInRootBuildingGM, DevConfigBuildingGMData>
    {
    }
}