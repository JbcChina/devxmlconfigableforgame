using System;
using System.Collections.Generic;

namespace XmlConfigable
{
    using static XmlDefine;

    public class CommonRootData : RootConfigBase
    { }

    [XmlHandlerRootAttribure(XmlRootType.Common, XmlLoadPriority.priorityHigh, "CommonRoot模块")]
    public class CommonRootHandler : RootHandlerBase<CommonRootHandler, CommonRootData>
    {
        public bool TrusteeState(Common_ModuleType commonModuleType, int TrusteeshipId)
        {
            var commonHandler = allModuleInRootHandlers[XmlRootType.Common];
            var curHandler = commonHandler[commonModuleType] as IXmlHandlerGetTrusteeshiPachage<TrusteeshipPackageBase>;
            return curHandler.GetTrusteeshipPackage(TrusteeshipId).active;
        }


        public override IXmlProxyExportAndLoad GetXmlIOProxy()
        {
            return new ExportAndLoadProxy(XmlConfigurableUtils.GetDirectryPath(xmlRootType),"ZZ_CommonRoot");
        }

    }



}