using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace XmlConfigable
{
    public interface UIFrameRefreshProxy<in element>
        where element : ModuleInRootConfigBase
    {
        void RefreshUIElement(element config);
    }

    public interface UIFrameRefreshFlow<out element>
        where element : ModuleInRootConfigBase
    {
        element GetCurConfig();
    }

    public interface UIFrameRefresh
    {
        void FrameRefreshElement();
    }


    public class UIAgantRuntime : DataNeedCache
    {
        static UIAgantRuntime agant;
        public static UIAgantRuntime Agant { get { if (agant == null) agant = new(); return agant; } }
        public override void Clear()
        {
            agant = new();
        }
        #region 每帧刷新的ui元素

        Dictionary<ModuleInRootConfigBase, UIFrameRefreshProxy<ModuleInRootConfigBase>> allProxies = new();
        public void AddProxy(ModuleInRootConfigBase config, UIFrameRefreshProxy<ModuleInRootConfigBase> proxy)
        {
            allProxies.Add(config, proxy);
        }

        public void RefreshUI()
        {
            foreach (var item in allProxies)
            {
                var config = item.Key;
                var proxy = item.Value;
                proxy?.RefreshUIElement(config);
            }
        }
        #endregion 每帧刷新的ui元素
    }





}