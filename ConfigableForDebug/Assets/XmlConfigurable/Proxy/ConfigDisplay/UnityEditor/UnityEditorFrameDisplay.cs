using System;
using System.Collections.Generic;
using UnityEngine;

namespace XmlConfigable
{
    public interface GetCustomFrameDisplayProxy<in config>
        where config : ModuleInRootConfigBase
    {
        UIFrameRefresh GetFrameRefreshProxy(config config);
    }
    public enum LayoutType
    {
        horizontal,
        vertical,
    }

    public struct UIProperties
    {
        public int indent;
        public int width;
        public UIProperties(int indent)
        {
            this.indent = indent;
            width = 0;
        }
        public UIProperties(int indent, int width)
        {
            this.indent = indent;
            this.width = width;
        }
    }
    public static class UnityEditorUiUtils
    {
        public static void WrapLayout(LayoutType layout, Action action)
        {
            switch (layout)
            {
                case LayoutType.horizontal:
                    GUILayout.BeginHorizontal();
                    action();
                    GUILayout.EndHorizontal();
                    break;
                case LayoutType.vertical:
                    GUILayout.BeginVertical();
                    action();
                    GUILayout.EndVertical();
                    break;
                default:
                    break;
            }
        }
        public static void DrawBlock(int width, int height)
        {
            GUILayout.Label("", GetWidth(width), GetHeight(height));
        }
        public static void DrawBlock(UIProperties uiArgs)
        {
            DrawBlock(uiArgs.indent, 0);
        }

        
        static Dictionary<int, GUILayoutOption> withOptions = new();
        static Dictionary<int, GUILayoutOption> heightOptions = new();
        public static GUILayoutOption GetWidth(int width)
        {
            if (!withOptions.ContainsKey(width))
            {
                withOptions.Add(width, GUILayout.Width(width));
            }
            return withOptions[width];
        }
        public static GUILayoutOption GetHeight(int height)
        {
            if (!heightOptions.ContainsKey(height))
            {
                heightOptions.Add(height, GUILayout.Height(height));
            }
            return heightOptions[height];
        }

        public static GUILayoutOption GetStringWidth(string content, int extent = 0)
        {
            return GetWidth(content.Length + extent);
        }
    }




    public class FrameDisplayConfigDataCache : DataNeedCacheSingle<FrameDisplayConfigDataCache>
    {
        List<UIFrameRefresh> allFrameDataCache = new();

        public override void Clear()
        {
            allFrameDataCache.Clear();
        }

        public void AddFrameDisplay(UIFrameRefresh element)
        {
            allFrameDataCache.Add(element);
        }



    }

    public abstract class FrameDisplayConfigBase_UnityEditor<T> : FrameDisplayConfigBase<T>
        where T : ModuleInRootConfigBase
    {
        protected UIProperties uiArgs;
        private FrameDisplayConfigBase_UnityEditor(T config) : base(config) { }
        public FrameDisplayConfigBase_UnityEditor(T config, UIProperties uiArgs) : base(config)
        {
            this.uiArgs = uiArgs;
        }
    }

    public class FrameDisplayConfigSwitchRoot_UnityEditor : FrameDisplayConfigBase_UnityEditor<ModuleInRootConfigBase>
    {
        Action<bool> onStateChange;
        IEnumerable<UIFrameRefresh> subElement;
        public FrameDisplayConfigSwitchRoot_UnityEditor(ModuleInRootConfigBase config, IEnumerable<UIFrameRefresh> subElement, Action<bool> stateChange) : base(config, default)
        {
            onStateChange = stateChange;
            this.subElement = subElement;
        }

        public override void RefreshUIElement(ModuleInRootConfigBase config)
        {
            UnityEditorUiUtils.WrapLayout(LayoutType.vertical, () =>
            {
                var lastState = config.active;
                config.active = GUILayout.Toggle(config.active, config.moduleName, UnityEditorUiUtils.GetStringWidth(config.moduleName, 20));
                if (lastState != config.active)
                {
                    onStateChange?.Invoke(config.active);
                }
                if (config.active)
                {
                    foreach (var item in subElement)
                    {
                        UnityEditorUiUtils.WrapLayout(LayoutType.vertical, () =>
                        {
                            UnityEditorUiUtils.WrapLayout(LayoutType.horizontal, () =>
                            {
                                UnityEditorUiUtils.DrawBlock(30, 0);
                                item.FrameRefreshElement();
                            });
                        });
                    }
                }
            });

        }
    }





    public class FrameDisplayConfigSwitch_UnityEditor : FrameDisplayConfigBase_UnityEditor<ModuleInRootConfigBase>
    {
        Action<bool> onStateChange;
        public FrameDisplayConfigSwitch_UnityEditor(ModuleInRootConfigBase config, Action<bool> stateChange, UIProperties uiArgs = default) : base(config, uiArgs)
        {
            onStateChange = stateChange;
        }
        public override void RefreshUIElement(ModuleInRootConfigBase config)
        {
            UnityEditorUiUtils.WrapLayout(LayoutType.horizontal, () =>
            {
                var lastState = config.active;
                config.active = GUILayout.Toggle(config.active, config.moduleName, UnityEditorUiUtils.GetStringWidth(config.moduleName, 20));
                if (lastState != config.active)
                {
                    onStateChange?.Invoke(config.active);
                }
            });
        }
    }


    public class FrameDisplayConfigButton_UnityEditor : FrameDisplayConfigBase_UnityEditor<ModuleInRootConfigBase>
    {
        Action btnOnClick;
        public FrameDisplayConfigButton_UnityEditor(ModuleInRootConfigBase config, Action onClick, UIProperties uiArgs = default) : base(config, uiArgs) { btnOnClick = onClick; }
        public override void RefreshUIElement(ModuleInRootConfigBase config)
        {
            if (config.active)
            {
                UnityEditorUiUtils.WrapLayout(LayoutType.horizontal, () =>
                {
                    if (GUILayout.Button(config.moduleName, UnityEditorUiUtils.GetStringWidth(config.moduleName, 20)))
                    {
                        UnityEditorUiUtils.DrawBlock(uiArgs);
                        btnOnClick();
                    };
                });
            }
        }
    }


    public class FrameDisplayConfigStringInputAndButton_UnityEditor : FrameDisplayConfigBase_UnityEditor<ModuleInRootConfigBase>
    {
        Action<string> btnOnClick;
        string inputContent;
        int inputLength;
        public FrameDisplayConfigStringInputAndButton_UnityEditor(ModuleInRootConfigBase config, Action<string> onClick, UIProperties uiArgs = default) : base(config, uiArgs)
        {
            btnOnClick = onClick;
            this.inputLength = uiArgs.width;
        }
        public override void RefreshUIElement(ModuleInRootConfigBase config)
        {
            if (config.active)
            {
                UnityEditorUiUtils.WrapLayout(LayoutType.horizontal, () =>
                {
                    inputContent = GUILayout.TextArea(inputContent, inputLength);
                    if (GUILayout.Button(config.moduleName, UnityEditorUiUtils.GetStringWidth(config.moduleName, 20)))
                    {
                        btnOnClick(inputContent);
                    }
                });
            }
        }
    }

}