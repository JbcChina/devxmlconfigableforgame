using System;
using System.Collections.Generic;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using UnityEngine;

namespace XmlConfigable
{

    public class ExportAndLoadProxy : IXmlProxyExportAndLoad
    {
        string curXmlPath,xmlDirectory;
        ExportAndLoadProxy() { }
        public ExportAndLoadProxy(string xmlDirectory, string xmlName)
        {
            if (xmlName.EndsWith(".xml"))
            {
                this.xmlDirectory = xmlDirectory;
                curXmlPath = Path.Combine(xmlDirectory, xmlName);
            }
            else
            {
                this.xmlDirectory = xmlDirectory;
                curXmlPath = Path.Combine(xmlDirectory, xmlName + ".xml");
            }
        }

        public void Export(object xmlConfig, Type[] extraTypes = null)
        {
            if (extraTypes == null)
            {
                extraTypes = XmlConfigurableUtils.GetAllXmlTypes();
            }
#if UNITY_EDITOR
            if (!Directory.Exists(xmlDirectory))
            {
                Directory.CreateDirectory(xmlDirectory);
            }
            else
            {
                if (File.Exists(curXmlPath))
                {
                    File.Delete(curXmlPath);
                }
            }
           
            FileStream fileStream = new FileStream(curXmlPath, FileMode.Create, FileAccess.ReadWrite, FileShare.ReadWrite);
            StreamWriter streamWriter = new StreamWriter(fileStream, System.Text.Encoding.UTF8);
            XmlSerializer xmlSerializer = new XmlSerializer(xmlConfig.GetType(), extraTypes);
            xmlSerializer.Serialize(streamWriter, xmlConfig);
            streamWriter.Close();
            fileStream.Close();
            fileStream.Dispose();
            UnityEditor.AssetDatabase.Refresh();
#endif
        }
        public object LoadXml(Type xmlType)
        {
            if (!File.Exists(curXmlPath))
            {
                Debug.Log(curXmlPath + "   cant find xml");
                return null;
            }
            var content = File.ReadAllText(curXmlPath);
            XmlSerializer xs = new XmlSerializer(xmlType, XmlConfigurableUtils.GetAllXMlTypeList());
            StringReader textReader = new StringReader(content);
            using (XmlReader xmlReader = XmlReader.Create(textReader))
            {
                var ret = xs.Deserialize(xmlReader);
                xmlReader.Dispose();
                xmlReader.Close();

                (ret as XmlConfigBase)?.XmlConfigLoadComplete();

                return ret;
            }
        }
    }

}