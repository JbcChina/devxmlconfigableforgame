namespace XmlConfigable
{
   


    public abstract class FrameDisplayConfigBase<T> : UIFrameRefreshFlow<T>, UIFrameRefreshProxy<T>, UIFrameRefresh
        where T : ModuleInRootConfigBase
    {
        protected bool curActicve = false;
        public FrameDisplayConfigBase(T config) { curConfig = config; }

        T curConfig;
        public T GetCurConfig()
        {
            return curConfig;
        }
        public abstract void RefreshUIElement(T config);

        public void FrameRefreshElement()
        {
            RefreshUIElement(curConfig);
        }
    }


}