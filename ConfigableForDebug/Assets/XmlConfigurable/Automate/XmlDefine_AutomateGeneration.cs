

using System;
using System.Collections.Generic;
namespace XmlConfigable
{
    public partial class XmlDefine
    {
         

        public static Dictionary<XmlRootType, IXmlRootHandler<RootConfigBase>> allRootHandlers = new()
        {
            
			{XmlRootType.Building,			DevHandlerBuildingRoot.Instance},
			{XmlRootType.Common,			CommonRootHandler.Instance},         
        };

 

        public static Dictionary<XmlRootType, Dictionary<Enum, IXmlModuleInRootHandler<ModuleInRootConfigBase>>> allModuleInRootHandlers = new()
        {
            
			 { XmlRootType.Common,new() { 
						{ Common_ModuleType.Button ,			ButtonHandler.Instance},
						{ Common_ModuleType.Print ,			PrintDataHandler.Instance},
						{ Common_ModuleType.Switch ,			SwitchHandler.Instance}, } }, 

			 { XmlRootType.Building,new() { 
						{ Building_ModuleType.BuildingBearing ,			DevHandlerInRootBuildingBearing.Instance},
						{ Building_ModuleType.GM ,			DevHandlerInRootBuildingGM.Instance}, } }, 

        };
         
        


        public static Dictionary<Enum, IXmlRootHandler<RootConfigBase>> moduleType2RootHandler = new()
        {
            
			{ Building_ModuleType.T_BearingLog ,DevHandlerBuildingRoot.Instance},
        };



        
        public enum Common_ModuleType {
            
			start		=		1000,
			Button		=		1001,
			Print		=		1002,
			Switch		=		1003,
			end,
        }

        
        public enum Building_ModuleType {
            
			start		=		3000,
			BuildingBearing		=		3001,
			GM		=		3002,
			////Trusteeship_打头说明是独立的托管模块
			T_BearingLog		=		3003,
			end,
        }


    }
}
        