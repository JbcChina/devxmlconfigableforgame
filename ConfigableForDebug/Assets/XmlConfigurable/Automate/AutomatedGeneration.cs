using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Text;
using UnityEngine;

namespace XmlConfigable
{

    public static class AutomatedGeneration
    {


        public static string moduleType2RootHandlerTemplate = @"
        public static Dictionary<Enum, IXmlRootHandler<RootConfigBase>> moduleType2RootHandler = new()
        {{
            {0}
        }};
";


        public static string classTemplate = @"

using System;
using System.Collections.Generic;
namespace XmlConfigable
{{
    public partial class XmlDefine
    {{
         {0}
    }}
}}
        ";


        public static string rootType2HandlerElementTemplate = "\t\t\t{{{0},{1}.Instance}},";
        public static string allRootHandlersTemplate = @"
        public static Dictionary<XmlRootType, IXmlRootHandler<RootConfigBase>> allRootHandlers = new()
        {{
            {0}         
        }};";


        public static string moduleInRootTypeTemplate = @"        
        public enum {0} {{
            {1}
        }}";

        public static string moduleInRootElementTemplate = @" {{ {0},new() {{ {1} }} }}, ";

        public static string moduleInRootElementTemplateDepth = @"{{ {0} ,{1}.Instance}},";

        public static string allModuleInRootHandlersTemplate = @" 

        public static Dictionary<XmlRootType, Dictionary<Enum, IXmlModuleInRootHandler<ModuleInRootConfigBase>>> allModuleInRootHandlers = new()
        {{
            {0}
        }};
         
        ";

        public static void GenerationInstanceTypeMapping()
        {
            ExportData.Agent.Clear();


            var assembly = Assembly.GetAssembly(typeof(XmlHandlerRootAttribure));
            Type[] allType = assembly.GetExportedTypes();


            Dictionary<XmlHandlerRootAttribure, IXmlRootHandler<RootConfigBase>> rootHandlers = new();
            Dictionary<IXmlModuleInRootHandler<ModuleInRootConfigBase>, XmlHandlerModuleInRootAttribute> moduleInRootHandlers = new();
            //Dictionary<XmlRootType, List<XmlIndependentTrusteePackageAttribute>> dependentTrusteeshipPackage = new();

            Dictionary<XmlIndependentTrusteePackageSetAttribute, NeedTrusteeshipModuleInRootConfig> allTrusteeshipModule = new();
            Dictionary<XmlRootType, List<XmlIndependentTrusteePackageSetAttribute>> rootType2AllTrusteeshipModule = new();


            foreach (Type curType in allType)
            {
                if (!XmlConfigurableUtils.ConsistentNameSpeace(curType))
                {
                    continue;
                }
                var curAttributes = curType.CustomAttributes;
                foreach (CustomAttributeData curAttribute in curAttributes)
                {
                    if (curAttribute.AttributeType == typeof(XmlHandlerRootAttribure))
                    {
                        var curHandler = Activator.CreateInstance(curType);
                        Attribute[] allAttributes = Attribute.GetCustomAttributes(curType, typeof(XmlHandlerRootAttribure));
                        foreach (var attribute in allAttributes)
                        {
                            var convert = attribute as XmlHandlerRootAttribure;
                            var value = curHandler as IXmlRootHandler<RootConfigBase>;

                            if (convert == null || value == null)
                            {
                                Debug.LogError($"转换失败：：{attribute} => {typeof(XmlHandlerRootAttribure)}    |or|   {curHandler} => {typeof(IXmlRootHandler<RootConfigBase>)}");
                            }
                            if (!rootHandlers.ContainsKey(convert))
                            {
                                rootHandlers.Add(convert, value);
                            }
                            else
                            {
                                Debug.LogError($"重复了{curHandler}");
                            }
                        }
                    }
                    else if (curAttribute.AttributeType == typeof(XmlHandlerModuleInRootAttribute))
                    {
                        var curHandler = Activator.CreateInstance(curType);
                        Attribute[] allAttributes = Attribute.GetCustomAttributes(curType, typeof(XmlHandlerModuleInRootAttribute));
                        foreach (var attribute in allAttributes)
                        {
                            var convert = attribute as XmlHandlerModuleInRootAttribute;
                            var value = curHandler as IXmlModuleInRootHandler<ModuleInRootConfigBase>;

                            if (convert == null || value == null)
                            {
                                Debug.LogError($"转换失败：：{attribute} => {typeof(XmlHandlerModuleInRootAttribute)}    |or|   {curHandler} => {typeof(IXmlModuleInRootHandler<ModuleInRootConfigBase>)}");
                            }
                            if (!moduleInRootHandlers.ContainsKey(value))
                            {
                                moduleInRootHandlers.Add(value, convert);
                            }
                            else
                            {
                                Debug.LogError($"重复了{curHandler}");
                            }
                        }
                    }
                }
                Dictionary<XmlIndependentTrusteePackageSetAttribute, NeedTrusteeshipModuleInRootConfig> curMap = XmlConfigurableUtils. GetAllXmlIndependentTrusteePackageAttribute2TrusteeshipModuleInRootConfig(curType);
                if (curMap != null)
                {
                    foreach (var item in curMap)
                    {
                        allTrusteeshipModule.Add(item.Key, item.Value);
                        if (!rootType2AllTrusteeshipModule.ContainsKey(item.Key.curRootType))
                        {
                            rootType2AllTrusteeshipModule.Add(item.Key.curRootType, new());
                        }
                        rootType2AllTrusteeshipModule[item.Key.curRootType].Add(item.Key);
                    }
                }
            }


            #region class0

            StringBuilder sbRootHandler = new();
            foreach (var rootHandlerKV in rootHandlers)
            {//生成rootType到RootHandler的映射
                var rootHandler = rootHandlerKV.Value;
                var rootAttributte = rootHandlerKV.Key;
                rootHandler.SetRootType(rootAttributte.curRootType);
                sbRootHandler.Append("\n");
                sbRootHandler.Append(string.Format(rootType2HandlerElementTemplate, XmlConfigurableUtils.GetEnumFildStyle(rootAttributte.curRootType), "\t\t\t" + rootHandler.GetType().Name));
            }
            string class0 = string.Format(allRootHandlersTemplate, sbRootHandler.ToString());

            #endregion class0


            #region class1

            Dictionary<XmlRootType, List<IXmlModuleInRootHandler<ModuleInRootConfigBase>>> rootType2Modules = new();
            foreach (var rootHandlerKV in moduleInRootHandlers)
            {//这里要做个结构转换，方便下面数据的获取
                var rootHandler = rootHandlerKV.Key;
                var rootAttributte = rootHandlerKV.Value;
                if (!rootType2Modules.ContainsKey(rootAttributte.rootType))
                {
                    rootType2Modules.Add(rootAttributte.rootType, new());
                }
                if (!rootType2Modules[rootAttributte.rootType].Contains(rootHandler))
                {
                    rootType2Modules[rootAttributte.rootType].Add(rootHandler);
                }
            }

            //moduleInRootElementTemplate = @" {{ {0},new() {{ {1} }} }}, ";
            //moduleInRootElementTemplateDepth = @"{{ {1} ,{2}.Instance}},";
            StringBuilder sbEnumTypeContentEmement = new();
            StringBuilder sbEnumTypeContent = new();
            StringBuilder sbRootType2Modules = new();



            Dictionary<string, XmlRootType> TrusteeModule2RootHandlerMap = new();

            foreach (var curRootType2Modules in rootType2Modules)
            {
                var rootType = curRootType2Modules.Key;
                List<IXmlModuleInRootHandler<ModuleInRootConfigBase>> modules = curRootType2Modules.Value;

                //modules.Sort((x, y) => { return moduleInRootHandlers[x].priority - moduleInRootHandlers[y].priority; });

                var enumName = XmlConfigurableUtils.GetTrusteeModuleTypeName(rootType);

                int index = 0;
                sbEnumTypeContentEmement.Clear();
                sbEnumTypeContentEmement.Append($"\n\t\t\tstart\t\t=\t\t{XmlConfigurableUtils.GetModuleInRootTypeId(rootType, index)},");
                StringBuilder sbRootType2ModulesElement = new();
                foreach (IXmlModuleInRootHandler<ModuleInRootConfigBase> moduleHandler in modules)
                {//加入root里模块的类型
                    index++;
                    var moduleName = moduleInRootHandlers[moduleHandler].moduleName;
                    sbEnumTypeContentEmement.Append($"\n\t\t\t{moduleName}\t\t=\t\t{XmlConfigurableUtils.GetModuleInRootTypeId(rootType, index)},");

                    sbRootType2ModulesElement.Append($"\n\t\t\t\t\t\t{string.Format(moduleInRootElementTemplateDepth, enumName + "." + moduleName, "\t\t\t" + moduleHandler.GetType().Name)}");
                }

                if (rootType2AllTrusteeshipModule.ContainsKey(rootType))
                {
                    sbEnumTypeContentEmement.Append($"\n\t\t\t////Trusteeship_打头说明是独立的托管模块");
                    var TrusteeshipModuleLst = rootType2AllTrusteeshipModule[rootType];
                    foreach (var TrusteeshipModule in TrusteeshipModuleLst)
                    {
                        index++;
                        ExportData.Agent.TrusteePackageNameCheck(rootType, TrusteeshipModule.curTrusteePackageName);
                        var TrusteeModuleName = XmlConfigurableUtils.GetTrusteeModuleTypeItemName(TrusteeshipModule);

                        sbEnumTypeContentEmement.Append($"\n\t\t\t{TrusteeModuleName}\t\t=\t\t{XmlConfigurableUtils.GetModuleInRootTypeId(rootType, index)},");
                        TrusteeModule2RootHandlerMap.Add($"{enumName}.{TrusteeModuleName}", rootType);
                    }
                }

                sbEnumTypeContentEmement.Append($"\n\t\t\tend,");

                var rootTypeModule = string.Format(moduleInRootTypeTemplate, enumName, sbEnumTypeContentEmement.ToString());
                sbEnumTypeContent.Append($"\n{rootTypeModule}\n");

                sbRootType2Modules.Append($"\n\t\t\t{string.Format(moduleInRootElementTemplate, XmlConfigurableUtils.GetEnumFildStyle(rootType), sbRootType2ModulesElement)}\n");
            }
            var class1 = string.Format(allModuleInRootHandlersTemplate, sbRootType2Modules);


            StringBuilder sbTrusteeModule2RootHandlerMap = new();
            foreach (var TrusteeModule2RootHandler in TrusteeModule2RootHandlerMap)
            {
                foreach (KeyValuePair<XmlHandlerRootAttribure, IXmlRootHandler<RootConfigBase>> item in rootHandlers)
                {
                    if (item.Key.curRootType == TrusteeModule2RootHandler.Value)
                    {
                        var element = string.Format(moduleInRootElementTemplateDepth, TrusteeModule2RootHandler.Key, item.Value.GetType().Name);
                        sbTrusteeModule2RootHandlerMap.Append($"\n\t\t\t{element}");
                    }
                }
            }
            var class11 = string.Format(moduleType2RootHandlerTemplate, sbTrusteeModule2RootHandlerMap.ToString());

            #endregion class1


            #region class3

            #endregion class3



            StringBuilder sbClassContent = new();
            sbClassContent.Append($"\n{class0}\n");
            sbClassContent.Append($"\n{class1}\n");
            sbClassContent.Append($"\n{class11}\n");
            sbClassContent.Append($"\n{sbEnumTypeContent}\n");
            
            var classContent = string.Format(classTemplate, sbClassContent.ToString());

            if (!File.Exists(XmlDefine.AutomatedGenerationPath))
            {
                StreamWriter fileW = File.CreateText(XmlDefine.AutomatedGenerationPath);
                fileW.Close();
            }
            var curStreamWAll = new StreamWriter(XmlDefine.AutomatedGenerationPath, false);


            curStreamWAll.Write(classContent);
            curStreamWAll.Close();
            curStreamWAll.Dispose();

            UnityEditor.AssetDatabase.SaveAssets();
            UnityEditor.AssetDatabase.Refresh();

        }





    }
}