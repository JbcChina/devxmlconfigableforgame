using System;
using System.Collections.Generic;
using System.IO;
using System.Xml.Serialization;

namespace XmlConfigable
{
    public abstract class ModuleInRootConfigBase : XmlConfigBase
    {
        [XmlAttribute("Switch")]
        public bool active;
        [XmlAttribute("ConfingID")]
        public int configId;
        [XmlAttribute("ModuleName")]
        public string moduleName;
    }



}