using System;
using UnityEngine;

namespace XmlConfigable
{
    public abstract class RootHandlerBase<handler, config> : XmlHandlerBase<handler, config>, IXmlRootHandler<config>
        where handler : class, new()
        where config : RootConfigBase, new()
    {
        XmlRootType IRootHandlerLife.GetRootType()
        {
            var ret = xmlRootType;
            if (ret == XmlRootType.Error)
            {
                Debug.LogError("xmlRootType is  error");
            }
            return ret;
        }
        public override void OnExportDataInstance(config curCreateData)
        {
            curCreateData.rootId = (int)xmlRootType;
            curCreateData.rootName = xmlRootType.ToString();
        }
    }


}