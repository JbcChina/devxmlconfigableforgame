using System;

namespace XmlConfigable
{
    public abstract class ModuleInRootHandlerBase<handler, config> : XmlHandlerBase<handler, config>, IXmlModuleInRootHandler<config>
        where handler : class, new()
        where config : ModuleInRootConfigBase, new()
    {
        public Enum moduleType { get; private set; }
        public void SetModuleTypeInRoot(Enum moduleType)
        {
            this.moduleType = moduleType;
        }
        public override IXmlProxyExportAndLoad GetXmlIOProxy()
        {
            return new ExportAndLoadProxy(XmlConfigurableUtils.GetDirectryPath(xmlRootType), XmlConfigurableUtils.GetEnumName(moduleType));
        }
        public override void OnExportDataInstance(config curCreateData)
        {
            curCreateData.configId = (int)(ValueType)moduleType;
            curCreateData.moduleName = moduleType.ToString();
        }
    }

}