using System;
using System.Xml.Serialization;

namespace XmlConfigable
{
    public abstract class RootConfigBase : XmlConfigBase
    {
        [XmlAttribute("RootId")]
        public int rootId;
        [XmlAttribute("RootName")]
        public string rootName;
    }
}