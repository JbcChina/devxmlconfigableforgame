using System;
using System.Collections.Generic;
using System.IO;
using UnityEditor;
using UnityEngine;

namespace XmlConfigable
{
    public class XmlConfigurableSys : Singleton<XmlConfigurableSys>
    {
        public void InitSys()
        {
            DataNeedCache.ClearAllCache();

            foreach (KeyValuePair<XmlRootType, IXmlRootHandler<RootConfigBase>> rootHandlerKV in XmlDefine.allRootHandlers)
            {
                var rootHandler = rootHandlerKV.Value;
                var rootHandlerType = rootHandlerKV.Key;
                rootHandler.SetRootType(rootHandlerType);
            }

            foreach (KeyValuePair<XmlRootType, Dictionary<Enum, IXmlModuleInRootHandler<ModuleInRootConfigBase>>> allModulesInRoot in XmlDefine.allModuleInRootHandlers)
            {
                var rootHandlerType = allModulesInRoot.Key;
                var dicModules = allModulesInRoot.Value;
                foreach (KeyValuePair<Enum, IXmlModuleInRootHandler<ModuleInRootConfigBase>> moduleInRootKV in dicModules)
                {
                    var typeInRoot = moduleInRootKV.Key;
                    var module = moduleInRootKV.Value;
                    module.SetRootType(rootHandlerType);
                    module.SetModuleTypeInRoot(typeInRoot);
                }
            }
        }


        public void LoadXmlConfig()
        {
            InitSys();

            ///先加载通用的公共模块
            var commonHandler = XmlDefine.allRootHandlers[XmlRootType.Common];

            var xmlLoadLife = commonHandler as IXmlDataLoadLife;
            var handlerIOProxy = commonHandler as IXmlHandlerIOProxy;
            var config = handlerIOProxy.GetXmlIOProxy().LoadXml(commonHandler.GetDataType());
            xmlLoadLife.LoadXml(config as XmlConfigBase);

            ///加载通用的公共的子模块
            var allModuleHandlers = XmlDefine.allModuleInRootHandlers[XmlRootType.Common];
            foreach (var moduleHandlerKV in allModuleHandlers)
            {
                var moduleType = moduleHandlerKV.Key;
                var modulehandler = moduleHandlerKV.Value;

                var xmlModuleLoadLife = modulehandler as IXmlDataLoadLife;
                var moduleHandlerIOProxy = modulehandler as IXmlHandlerIOProxy;

                var curConfig = moduleHandlerIOProxy.GetXmlIOProxy().LoadXml(modulehandler.GetDataType());
                if (curConfig == null || curConfig as XmlConfigBase == null)
                {
                    Debug.LogError($"Load is null {modulehandler.GetDataType()}        result:{curConfig}");
                }
                xmlModuleLoadLife.LoadXml(curConfig as XmlConfigBase);
            }


            foreach (var item in XmlDefine.allRootHandlers)
            {
                var rootType = item.Key;
                var rootHandler = item.Value;
                if (rootType != XmlRootType.Common)
                {

                    var xmlModuleLoadLife = rootHandler as IXmlDataLoadLife;
                    var moduleHandlerIOProxy = rootHandler as IXmlHandlerIOProxy;

                    var curConfig = moduleHandlerIOProxy.GetXmlIOProxy().LoadXml(rootHandler.GetDataType());
                    xmlModuleLoadLife.LoadXml(curConfig as XmlConfigBase);
                }
            }


            foreach (var item in XmlDefine.allModuleInRootHandlers)
            {
                var rootType = item.Key;
                var moduleHandlers = item.Value;
                if (rootType != XmlRootType.Common)
                {
                    foreach (var moduleKV in moduleHandlers)
                    {
                        var moduleType = moduleKV.Key;
                        var moduleHandler = moduleKV.Value;

                        var xmlModuleLoadLife = moduleHandler as IXmlDataLoadLife;
                        var moduleHandlerIOProxy = moduleHandler as IXmlHandlerIOProxy;

                        var curConfig = moduleHandlerIOProxy.GetXmlIOProxy().LoadXml(moduleHandler.GetDataType());
                        xmlModuleLoadLife.LoadXml(curConfig as XmlConfigBase);
                    }
                }
            }

            XmlVoucherService.Agent.ChecAllVouckerOnXmlConfigLoadComplete();

        }


        public void ExportXmlData()
        {
            InitSys();

            HashSet<Type> hadRecord = new();

            foreach (var item in XmlDefine.allRootHandlers)
            {
                item.Value.GetExportData(true);
            }

            foreach (KeyValuePair<Enum, IXmlModuleInRootHandler<ModuleInRootConfigBase>> commonHandlerKv in XmlDefine.allModuleInRootHandlers[XmlRootType.Common])
            {///通用模块数据创建
                var commonHanlder = commonHandlerKv.Value;
                commonHanlder.GetExportData(true);
            }

            List<IXmlHandlerWithData<XmlConfigBase>> allHandlersForIOProxy = new();

            List<XmlConfigBase> allExportDatas = new();


            foreach (KeyValuePair<XmlRootType, Dictionary<Enum, IXmlModuleInRootHandler<ModuleInRootConfigBase>>> allModulesInRoot in XmlDefine.allModuleInRootHandlers)
            {//这里导出的是非通用模块的子模块
                if (allModulesInRoot.Key == XmlRootType.Common)
                {
                    continue;
                }
                foreach (KeyValuePair<Enum, IXmlModuleInRootHandler<ModuleInRootConfigBase>> moduleInRootKV in allModulesInRoot.Value)
                {
                    var typeInRoot = moduleInRootKV.Key;
                    var moduleHandler = moduleInRootKV.Value;
                    var handlerIOProxy = moduleHandler as IXmlHandlerIOProxy;

                    ModuleInRootConfigBase data = moduleHandler.GetExportData(true);

                    Dictionary<XmlIndependentTrusteePackageSetAttribute, NeedTrusteeshipModuleInRootConfig> needTrusteeshipPackageLstData = XmlConfigurableUtils.GetAllXmlIndependentTrusteePackageAttribute2TrusteeshipModuleInRootConfig(data.GetType());
                    hadRecord.Add(data.GetType());

                    Dictionary<XmlIndependentTrusteePackageSetAttribute, NeedTrusteeshipModuleInRootConfig> needTrusteeshipPackageLstHandler = XmlConfigurableUtils.GetAllXmlIndependentTrusteePackageAttribute2TrusteeshipModuleInRootConfig(moduleHandler.GetType());
                    hadRecord.Add(moduleHandler.GetType());

                    AddNeedTrusteeModule(needTrusteeshipPackageLstData, data);
                    AddNeedTrusteeModule(needTrusteeshipPackageLstHandler, data);

                    (data as IXmlDataInitOnExportBefore)?.InitXmlExportData();

                    allExportDatas.Add(data);

                    allHandlersForIOProxy.Add(moduleHandler);

                }
            }

            var assembly = System.Reflection.Assembly.GetAssembly(typeof(XmlHandlerRootAttribure));
            Type[] allType = assembly.GetExportedTypes();

            foreach (var curType in allType)
            {
                if (hadRecord.Contains(curType))
                {
                    continue;
                }
                Dictionary<XmlIndependentTrusteePackageSetAttribute, NeedTrusteeshipModuleInRootConfig> curMap = XmlConfigurableUtils.GetAllXmlIndependentTrusteePackageAttribute2TrusteeshipModuleInRootConfig(curType);
                if (curMap != null )
                {
                    foreach (var item in curMap)
                    {
                        var moduleAttrite = item.Key;
                        var needTrusteeModule = item.Value;

                        needTrusteeModule.InitXmlExportData();
                        var curRootHandler = XmlDefine.allRootHandlers[moduleAttrite.curRootType];
                        if (curRootHandler.GetExportData(false) == null)
                        {
                            Debug.LogError($"RootData Is Null      {curRootHandler.GetDataType()}     {curRootHandler.GetRootType()}");
                            continue;
                        }
                        if (curRootHandler.GetExportData(false).holdingVouchers == null)
                        {
                            curRootHandler.GetExportData(false).holdingVouchers = new();
                        }
                        curRootHandler.GetExportData(false).holdingVouchers.Add(needTrusteeModule);


                        needTrusteeModule.ModuleName = item.Key.curTrusteePackageName;
                        needTrusteeModule.ConfigID = GetTrusteeModuleConfigId(moduleAttrite);

                    }
                    hadRecord.Add(curType);
                }
            }


            foreach (KeyValuePair<Enum, IXmlModuleInRootHandler<ModuleInRootConfigBase>> commonHandlerKv in XmlDefine.allModuleInRootHandlers[XmlRootType.Common])
            {///通用模块数据导出
                var commonHanlder = commonHandlerKv.Value;
                var commonData = commonHanlder.GetExportData(false);
                (commonData as IXmlDataInitOnExportBefore)?.InitXmlExportData();

                allHandlersForIOProxy.Add(commonHanlder);
                allExportDatas.Add(commonData);
            }   

            foreach (var type2Handlder in XmlDefine.allRootHandlers)
            {///这里主要做数据导出前，有初始化需求的时候。进行调用
                //XmlRootType rootType = type2Handlder.Key;
                IXmlRootHandler<RootConfigBase> rootHandler = type2Handlder.Value;
                var rootData = rootHandler.GetExportData(true);
                (rootData as IXmlDataInitOnExportBefore)?.InitXmlExportData();

                allHandlersForIOProxy.Add(rootHandler);
                //allIOProxies.Add((rootHandler as IXmlHandlerIOProxy).GetXmlIOProxy());
            }


            foreach (var exportData in allExportDatas)
            {///适配 需要外部托管模块的数据 来辅助完成功能。
                var enumLst_ModuleType = (exportData as FitExternalTrutershipModule)?.GetExternalTrusteeshipModulesType();
                if (enumLst_ModuleType != null)
                {
                    exportData.externalVouchers = new();
                    foreach (var moduleType in enumLst_ModuleType)
                    {
                        var record = XmlVoucherService.Agent.GetTrusteeshipRecord(moduleType);
                        if (record == null)
                        {
                            Debug.LogError($"获取托管模块失败。{exportData} => GetTrusteeshipRecord:{moduleType}");
                            continue;
                        }
                        exportData.externalVouchers.Add(record.CurVoucher);
                    }

                    var allCustomAttributes = exportData.GetType().GetCustomAttributes(false);
                    if (allCustomAttributes != null)
                    {
                        foreach (var atteibute in allCustomAttributes)
                        {
                            if (atteibute is XmlLoadTrusteePackageModule)
                            {
                                foreach (var trusteeType in (atteibute as XmlLoadTrusteePackageModule).CurTrusteeTypes)
                                {
                                    var record = XmlVoucherService.Agent.GetTrusteeshipRecord(trusteeType);
                                    if (record == null)
                                    {
                                        Debug.LogError($"获取托管模块失败。{atteibute} => trusteeType:{trusteeType}");
                                        continue;
                                    }
                                    exportData.externalVouchers.Add(record.CurVoucher);
                                }
                            }
                        }
                    }
                }
            }

            foreach (IXmlHandlerWithData<XmlConfigBase> handler in allHandlersForIOProxy)
            {///统一导出
                var ioProxy = (handler as IXmlHandlerIOProxy).GetXmlIOProxy();
                ioProxy.Export(handler.GetData());
            }


            //AssetDatabase.SaveAssets();
            AssetDatabase.Refresh();

            Debug.LogError("导出完成");

        }


        void AddNeedTrusteeModule(Dictionary<XmlIndependentTrusteePackageSetAttribute, NeedTrusteeshipModuleInRootConfig> needTrusteeshipPackageLst, ModuleInRootConfigBase host)
        {
            if (needTrusteeshipPackageLst != null)
            {
                foreach (KeyValuePair<XmlIndependentTrusteePackageSetAttribute, NeedTrusteeshipModuleInRootConfig> item in needTrusteeshipPackageLst)
                {
                    var TrusteeshipAttribute = item.Key;

                    var TrusteeshipConfig = item.Value;
                    TrusteeshipConfig.ModuleName = TrusteeshipAttribute.curTrusteePackageName;
                    TrusteeshipConfig.ConfigID = GetTrusteeModuleConfigId(TrusteeshipAttribute);
                    TrusteeshipConfig.InitXmlExportData();

                    if (host.holdingVouchers == null)
                    {
                        host.holdingVouchers = new();
                    }
                    host.holdingVouchers.Add(TrusteeshipConfig);
                }
            }
        }


        int GetTrusteeModuleConfigId(XmlIndependentTrusteePackageSetAttribute attribute)
        {
            var typeName = $"XmlConfigable.XmlDefine+{XmlConfigurableUtils.GetTrusteeModuleTypeName(attribute.curRootType)}";
            var ennumType = Type.GetType(typeName);
            if ((ennumType == null))
            {
                Debug.LogError($"没有找到类型：{typeName}     {attribute.curRootType}:{attribute.curTrusteePackageName}");
                return -1;
            }
            var enumValue = Enum.Parse(ennumType, XmlConfigurableUtils.GetTrusteeModuleTypeItemName(attribute));
            if (enumValue == null)
            {
                Debug.LogError($"没有找到类型的值：{XmlConfigurableUtils.GetTrusteeModuleTypeItemName(attribute)}     {attribute.curRootType}:{attribute.curTrusteePackageName}");
                return -1;
            }
            return Convert.ToInt32(enumValue);
        }


    }
}