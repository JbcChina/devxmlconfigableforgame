using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Xml.Serialization;
namespace XmlConfigable
{

    public static partial class XmlConfigurableUtils
    {
        public static void DefaultDisplay()
        {
            //FrameDisplayConfigDataCache.Agent.AddFrameDisplay()
        }


        public static int GetModuleInRootTypeId(XmlRootType rootType, ValueType moduleInRootType)
        {
            return ((int)rootType) * 1000 + ((int)moduleInRootType);
        }

        public static int EncodeTrusteeshipId(ValueType commonModuleType, int lstId)
        {
            return ((int)commonModuleType) * 100000 +  + lstId;
        }

        public static string GetEnumFildStyle(Enum v)
        {
            return v.GetType().Name + "." + v;
        }

        public static bool ConsistentNameSpeace(Type checkType)
        {
            return checkType.Namespace == "XmlConfigable";
        }


        public static string GetEnumName(Enum enumType)
        {
            var tostring = enumType.ToString();
            var startIndex = tostring.LastIndexOf(".") + 1;
            var name = tostring.Substring(startIndex, tostring.Length - startIndex);
            return name;
        }



        public static string GetDirectryPath(XmlRootType rootType)
        {
            var dir = XmlDefine.ExportRootDirectory;
            var name = GetEnumName(rootType);
            var subDir = $"{rootType.GetType().Name}/{name}";
            var ret = Path.Combine(dir, subDir);
            if (!Directory.Exists(ret))
            {
                Directory.CreateDirectory(ret);
            }
            return ret;
        }


        /// <summary>
        /// 检测目录，没有的话，创建目录
        /// </summary>
        /// <param name="path"></param>
        public static void CheckIsNullAndCreateDirectory(string path)
        {
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }
        }

        public static string GetTrusteeModuleTypeName(XmlRootType rootType)
        {
            return rootType + "_ModuleType";
        }

        public static string GetTrusteeModuleTypeItemName(XmlIndependentTrusteePackageSetAttribute attribute)
        {
            return "T_" + attribute.curTrusteePackageName;
        }

      public  static Dictionary<XmlIndependentTrusteePackageSetAttribute, NeedTrusteeshipModuleInRootConfig> GetAllXmlIndependentTrusteePackageAttribute2TrusteeshipModuleInRootConfig(Type classType)
        {

            var defaultConstructor = classType.GetConstructor(Type.EmptyTypes);
            if (defaultConstructor == null || classType.IsAbstract || classType.IsGenericType || classType.IsConstructedGenericType || classType
                .Namespace != "XmlConfigable")
            {
                return null;
            }

            var ret = new Dictionary<XmlIndependentTrusteePackageSetAttribute, NeedTrusteeshipModuleInRootConfig>();
            var classInstance = Activator.CreateInstance(classType);

            var fileds = classType.GetFields();
            for (int i = 0; i < fileds.Length; i++)
            {
                var curFiled = fileds[i];
                if (curFiled.FieldType == typeof(NeedTrusteeshipModuleInRootConfig))
                {
                    XmlIndependentTrusteePackageSetAttribute attribute = curFiled.GetCustomAttribute<XmlIndependentTrusteePackageSetAttribute>();
                    if (attribute != null)
                    {
                        var filedV = curFiled.GetValue(classInstance) as NeedTrusteeshipModuleInRootConfig;
                        ret.Add(attribute, filedV);
                    }
                }
            }

            var properties = classType.GetProperties();
            for (int i = 0; i < properties.Length; i++)
            {
                var curProperty = properties[i];
                if (curProperty.PropertyType == typeof(NeedTrusteeshipModuleInRootConfig))
                {
                    XmlIndependentTrusteePackageSetAttribute attribute = curProperty.GetCustomAttribute<XmlIndependentTrusteePackageSetAttribute>();
                    if (attribute != null)
                    {
                        var filedV = curProperty.GetValue(classInstance) as NeedTrusteeshipModuleInRootConfig;
                        ret.Add(attribute, filedV);
                    }
                }
            }


            return ret;
        }

        /// <summary>
        /// 返回一个排好序的SysHandlers
        /// </summary>
        /// <returns></returns>
        public static Type[] GetAllXmlTypes()
        {
            var baseType = typeof(XmlConfigBase);
            var ienum = baseType.Assembly.GetTypes();
            List<Type> retLst = new();
            foreach (var item in ienum)
            {
                if (item.IsSubclassOf(baseType) && !item.IsAbstract && item.IsClass && item.IsPublic)
                {
                    retLst.Add(item);
                }
            }
            return retLst.ToArray();
        }
        /// <summary>
        /// 返回一个排好序的SysHandlers
        /// </summary>
        /// <returns></returns>
        public static Type[] GetAllXmlTypes2()
        {
            var baseType = typeof(XmlTypeAttribute);
            var ienum = baseType.Assembly.GetTypes();
            List<Type> retLst = new();
            foreach (var item in ienum)
            {
                if (item.IsSubclassOf(baseType) && !item.IsAbstract && item.IsClass && item.IsPublic)
                {
                    retLst.Add(item);
                }
            }
            return retLst.ToArray();
        }



        static List<Type> typeLst;
        public static Type[] GetAllXMlTypeList()
        {
            if (typeLst == null)
            {
                typeLst = new List<Type>();
                foreach (var item in Assembly.GetExecutingAssembly().GetTypes())
                {
                    if (item.IsClass)
                    {
                        foreach (var attr in System.Attribute.GetCustomAttributes(item, true))
                        {
                            if (attr is XmlTypeAttribute)
                            {
                                var name = item.AssemblyQualifiedName;
                                typeLst.Add(Type.GetType(name));
                                break;
                            }
                        }
                    }
                }
            }
            return typeLst.ToArray();
        }
    }

}


  

