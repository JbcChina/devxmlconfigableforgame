using System;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

namespace XmlConfigable
{
    public enum XmlRootType
    {
        Error,
        Common,
        GameUI,
        Building
    }


    public static partial class XmlDefine
    {
        public static string ExportRootDirectory { get => Path.Combine(Application.streamingAssetsPath,"NewConfigure"); }

        public static string AutomatedGenerationPath
        {
            get
            {
                var path = Path.Combine(Directory.GetCurrentDirectory(), "Assets/XmlConfigurable/Automate/");
                if (!Directory.Exists(path))
                {
                    Directory.CreateDirectory(path);
                }
                return Path.Combine(path, "XmlDefine_AutomateGeneration.cs");
            }
        }

        public static IXmlModuleInRootHandler<ModuleInRootConfigBase> GetCommonHandler(Common_ModuleType commonType)
        {
            var allHandlers = allModuleInRootHandlers[XmlRootType.Common];
            return allHandlers[commonType];
        }


        //public enum Common_ModuleType { }
    }
}