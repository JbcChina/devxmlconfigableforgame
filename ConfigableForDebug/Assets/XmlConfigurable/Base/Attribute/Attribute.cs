using System;
using System.Collections.Generic;
using System.Xml.Serialization;
using UnityEngine;

namespace XmlConfigable
{
    public class XmlLoadPriority : Attribute
    {
        public const int priorityHigh = 5000000;
        public const int priorityMidddle = 3000000;
        public const int priorityLow = 1000000;

        public int loadPriority { get; private set; }
        XmlLoadPriority() { }
        public XmlLoadPriority(int loadPriority) { this.loadPriority = loadPriority; }

    }


    [AttributeUsage(AttributeTargets.Class, Inherited = false, AllowMultiple = false)]
    public class XmlHandlerRootAttribure : XmlLoadPriority
    {
        public string tips { get; private set; }
        public XmlRootType curRootType { get; private set; }

        public XmlHandlerRootAttribure(XmlRootType rootType, int _loadPriority, string tips = null) : base(_loadPriority)
        {
            curRootType = rootType;
            this.tips = tips;
        }
    }

    [AttributeUsage(AttributeTargets.Class, Inherited = false, AllowMultiple = true)]
    public class XmlHandlerModuleInRootAttribute : Attribute
    {
        public XmlRootType rootType { get; private set; }
        public string moduleName { get; private set; }

        public XmlHandlerModuleInRootAttribute(XmlRootType rootType, string moduleName)
        {
            this.rootType = rootType;
            this.moduleName = moduleName;
        }
    }

    //[AttributeUsage(AttributeTargets.Class)]
    //public class XmlDataRootAttribute : XmlLoadPriority
    //{
    //    public XmlRootType rootType { get; private set; }

    //    public XmlDataRootAttribute(XmlRootType rootType, int loadPriority = priorityMidddle) : base(loadPriority)
    //    {
    //        this.rootType = rootType;
    //    }
    //}


    //[AttributeUsage(AttributeTargets.Class)]
    //public class XmlDataModuleInRootAttribute : XmlLoadPriority
    //{
    //    public XmlRootType curRootType { get; private set; }
    //    public string moduleName { get; private set; }

    //    public XmlDataModuleInRootAttribute(XmlRootType curRootType, string moduleName, int loadPriority = priorityLow) : base(loadPriority)
    //    {
    //        ExportData.CurExport.ModuleInRootNameCheck(curRootType,moduleName);
    //        this.curRootType = curRootType;
    //        this.moduleName = moduleName;
    //    }
    //}

    /// <summary>
    /// 只能标记有默认构造函数里的Property和Field
    /// </summary>
    [AttributeUsage(AttributeTargets.Property | AttributeTargets.Field, Inherited = false, AllowMultiple = true)]
    public class XmlIndependentTrusteePackageSetAttribute : XmlLoadPriority
    {
        public string curTrusteePackageName { get; protected set; }
        public XmlRootType curRootType { get; protected set; }

        public XmlIndependentTrusteePackageSetAttribute(XmlRootType rootType, string _curTrusteePackageName, int loadPriority = priorityMidddle) : base(loadPriority)
        {
            ExportData.Agent.TrusteePackageNameCheck(rootType, _curTrusteePackageName + "_AttributeCheck");
            curTrusteePackageName = _curTrusteePackageName;
            this.curRootType = rootType;
        }
    }

    /// <summary>
    /// T_开头的都是别的模块的托管模块。才能进行当前 特性索引
    /// </summary>
    [AttributeUsage(AttributeTargets.Class)]
    public class XmlLoadTrusteePackageModule : Attribute
    {
        public int[] CurTrusteeTypes { get; private set; }
        public XmlLoadTrusteePackageModule(params int[] trusteeTypesWillLoad) 
        {
            CurTrusteeTypes = trusteeTypesWillLoad;
        }
    }
}