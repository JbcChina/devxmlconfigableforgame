using System;
using System.Collections.Generic;
using System.Reflection;
using UnityEngine;

namespace XmlConfigable
{

    public abstract class DataNeedCache
    {
        public DataNeedCache()
        {
            allDataCache.Add(this);
        }

        static List<DataNeedCache> allDataCache = new();

        /// <summary>
        /// 清理所有缓存。有缓存的数据都会清理。没缓存的也不需要清理
        /// </summary>
        public static void ClearAllCache()
        {
            for (int i = 0; i < allDataCache.Count; i++)
            {
                allDataCache[i].Clear();
            }
        }

        public abstract void Clear();
    }

    public abstract class DataNeedCacheSingle<T> : DataNeedCache
        where T : new()
    {

        static T agent = new();
        public static T Agent { get => agent; }

        public override void Clear()
        {
            agent = new();
        }
    }

    public class ExportData : DataNeedCacheSingle<ExportData>
    {
        public ExportData() { }

        Dictionary<XmlRootType, HashSet<string>> trusteePackageName = new();
        public void TrusteePackageNameCheck(XmlRootType root, string name)
        {
            //Debug.LogError($"{root}");
            if (!trusteePackageName.ContainsKey(root))
            {
                trusteePackageName.Add(root, new());
            }
            if (trusteePackageName[root].Contains(name))
            {
                Debug.LogError($"TrusteePackageNameCheck 重复，{root} 请检查: {name}");
                return;
            }
            trusteePackageName[root].Add(name);
        }


        Dictionary<XmlRootType, HashSet<string>> ModuleInRootName = new();
        public void ModuleInRootNameCheck(XmlRootType root, string name)
        {
            if (!ModuleInRootName.ContainsKey(root))
            {
                ModuleInRootName.Add(root, new());
            }
            if (ModuleInRootName[root].Contains(name))
            {
                Debug.LogError($"ModuleInRootNameCheck 重复，{root} 请检查: {name}");
                return;
            }
            ModuleInRootName[root].Add(name);
        }
        
    }


    public class XmlVoucherService : DataNeedCacheSingle<XmlVoucherService>
    {
        public XmlVoucherService() { }

        Dictionary<int, TrusteeshipRecord> configId2TrusteeshipRecord = new();

        public TrusteeshipRecord GetTrusteeshipRecord(ValueType configId)
        {
            if (!configId2TrusteeshipRecord.ContainsKey((int)configId))
            {
                Debug.LogError($"没有发现存据{configId}");
                return null;
            }
            return configId2TrusteeshipRecord[(int)configId];
        }
        public TrusteeshipRecord GetTrusteeshipRecord(int configId)
        {
            if (!configId2TrusteeshipRecord.ContainsKey(configId))
            {
                Debug.LogError($"没有发现存据{configId}");
                return null;
            }
            return configId2TrusteeshipRecord[(int)configId];
        }

        public void TrutreeshipRecordLoad(TrusteeshipRecord record, TrusteeVoucher voucher)
        {
            if (record != null && voucher != null)
            {
                if (!configId2TrusteeshipRecord.ContainsKey(voucher.configId))
                {
                    configId2TrusteeshipRecord.Add(voucher.configId, record);
                }
                else
                {
                    Debug.LogError($"两个不同的凭据指向了一个存据:{voucher.configId}");
                }
                CheckVoucher(voucher);
            }
        }


        List<TrusteeVoucher> allVouchers = new();

        public void VocherRecord(TrusteeVoucher voucher)
        {
            allVouchers.Add(voucher);
        }

        public void ChecAllVouckerOnXmlConfigLoadComplete()
        {
            foreach (var voucher in allVouchers)
            {
                CheckVoucher(voucher);
            }
        }

        void CheckVoucher(TrusteeVoucher voucher)
        {
            if (!voucher.active)
            {
                return;
            }
            if (!configId2TrusteeshipRecord.ContainsKey(voucher.configId))
            {
                Debug.LogError($"凭据没有对应的存据：{voucher.configId}  {voucher.moduleName}");
            }
            else
            {
                var record = configId2TrusteeshipRecord[voucher.configId];
                for (int i = 0; i < record.commonTrusteeIds.Length; i++)
                {
                    var curTrusteesId = record.commonTrusteeIds[i];
                    var handler = XmlDefine.allModuleInRootHandlers[XmlRootType.Common][(XmlDefine.Common_ModuleType)record.commonTypeLst[i]];

                    var commonData = handler.GetData() as CommonTrusteeData;
                    var curPackage = commonData.GetTrusteeshipPackage(curTrusteesId) as ITrusteeshipDataOperation<TrusteeshipElementInPackageBase>;

                    //var lst = curPackage.GetElements();

                    var element = curPackage.GetElementByConfigId(voucher.configId);
                    if (element.moduleName != voucher.moduleName)
                    {
                        Debug.LogError($"凭据的名称不一致，但是configId一致  voucher:{voucher.moduleName}    {element.moduleName}");
                    }

                    element.active = voucher.active;
                }
            }



        }
    }




}