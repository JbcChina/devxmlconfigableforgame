using System;
using System.Collections.Generic;

namespace XmlConfigable
{

    public class SwitchElement : TrusteeshipElementInPackageBase
    {

    }

    public class SwitchTrusteeshipPackage : TrusteeshipPackageBase<SwitchElement>
    {
        public List<SwitchElement> switchLst = new();

        public override void AddTrutrrshipElement(SwitchElement element)
        {
            switchLst.Add(element);
        }

        public override IEnumerable<SwitchElement> GetElements()
        {
            return switchLst;
        }
    }

    //[XmlDataModuleInRoot(XmlRootType.Common, "Switch")]
    public class SwitchData : CommonTrusteeData
    {
    }

    [XmlHandlerModuleInRoot(XmlRootType.Common, "Switch")]
    public class SwitchHandler : CommonTrusteeHandler<SwitchHandler, SwitchData, SwitchTrusteeshipPackage, SwitchElement>
    {


    }





}