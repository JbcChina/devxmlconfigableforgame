using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace XmlConfigable
{

    public class ButtonElement : TrusteeshipElementInPackageBase
    {

    }

    public class ButtonTrusteeshipPackage : TrusteeshipPackageBase<ButtonElement>
    {
        public List<ButtonElement> ButtonLst = new();

        public override void AddTrutrrshipElement(ButtonElement element)
        {
            ButtonLst.Add(element);
        }

        public override IEnumerable<ButtonElement> GetElements()
        {
            return ButtonLst;
        }
    }

    [XmlRoot("XmlRootButtonData")]
    public class ButtonData : CommonTrusteeData
    {
    }

    [XmlHandlerModuleInRoot(XmlRootType.Common, "Button")]
    public class ButtonHandler : CommonTrusteeHandler<ButtonHandler, ButtonData, ButtonTrusteeshipPackage, ButtonElement>
    {


    }





}