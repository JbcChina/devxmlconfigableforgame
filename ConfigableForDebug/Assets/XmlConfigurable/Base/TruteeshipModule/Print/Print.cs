using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace XmlConfigable
{
    [XmlType("日志托管项")]
    public class PrintElement: TrusteeshipElementInPackageBase
    {

    }
    [XmlType("日志托管模块")]
    public class PrintTrusteeshipPackage : TrusteeshipPackageBase<PrintElement>
    {
        [XmlElement("LogItem")]
        public List<PrintElement> logLst = new();

        public override void AddTrutrrshipElement(PrintElement element)
        {
            logLst.Add(element);
        }
        public override IEnumerable<PrintElement> GetElements()
        {
            return logLst;
        }
    }
    [XmlRoot("XmlRootPrintData")]
    public class PrintData : CommonTrusteeData
    {

    }

    [XmlHandlerModuleInRoot(XmlRootType.Common, "Print")]
    public class PrintDataHandler : CommonTrusteeHandler<PrintDataHandler, PrintData, PrintTrusteeshipPackage, PrintElement>
    {
        
    }






}