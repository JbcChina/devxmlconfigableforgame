using System;
using System.Collections.Generic;
using System.IO;
using System.Xml.Serialization;
using UnityEngine;

namespace XmlConfigable
{
    public interface IXmlHandlerGenerationTrusteeshipIdOnExport<in TrusteeshipPackage, element>
        where TrusteeshipPackage : TrusteeshipPackageBase
    {
        public int GenerationTrusteeshiId(TrusteeshipPackage package);
    }

    public interface IXmlHandlerGetTrusteeshiPachage<out TrusteeshipPackage>
        where TrusteeshipPackage : TrusteeshipPackageBase
    {
        public TrusteeshipPackage GetTrusteeshipPackage(int TrusteeshipId);
    }

    public interface ITrusteeshipPackageType
    {
        Type GetTrusteeshipPackageType();
    }


    public abstract class CommonTrusteeHandler<handler, config, TrusteeshipPackage, TrusteeshipElement> :
        ModuleInRootHandlerBase<handler, config>,
        ITrusteeshipPackageType,
        IXmlHandlerGetTrusteeshiPachage<TrusteeshipPackage>
        where handler : class, new()
        where config : CommonTrusteeData, new()
        where TrusteeshipPackage : TrusteeshipPackageBase, ITrusteeshipDataOperation<TrusteeshipElement>
        where TrusteeshipElement : TrusteeshipElementInPackageBase
    {
        public TrusteeshipPackage GetTrusteeshipPackage(int TrusteeshipId)
        {
            return CurData.GetTrusteeshipPackage(TrusteeshipId) as TrusteeshipPackage;
        }

        public Type GetTrusteeshipPackageType()
        {
            return typeof(TrusteeshipPackage);
        }

        public override IXmlProxyExportAndLoad GetXmlIOProxy()
        {
            return new ExportAndLoadProxy(XmlConfigurableUtils.GetDirectryPath(xmlRootType), XmlConfigurableUtils.GetEnumName(moduleType));
        }


        public override void OnXmlLoadCompleted()
        {
            foreach (TrusteeshipPackageBase elePackage in CurData.curTrusteeshipLst)
            {
                TrusteeshipPackage convert = elePackage as TrusteeshipPackage;
                IEnumerable<TrusteeshipElement> elements = convert.GetElements();

                List<UIFrameRefresh> subElements = new();

                var customFrameRefresh = this as GetCustomFrameDisplayProxy<TrusteeshipElement>;
                foreach (TrusteeshipElement element in elements)
                {
                    UIFrameRefresh frameRefreshProxy = null;
                    if (customFrameRefresh == null)
                    {
                        frameRefreshProxy = new FrameDisplayConfigSwitch_UnityEditor(element, null);
                    }
                    else
                    {
                        frameRefreshProxy = customFrameRefresh.GetFrameRefreshProxy(element);
                    }
                    subElements.Add(frameRefreshProxy);
                }
                FrameDisplayConfigDataCache.Agent.AddFrameDisplay(new FrameDisplayConfigSwitchRoot_UnityEditor(elePackage, subElements, null));
            }
        }

    }

    public abstract class CommonTrusteeData : ModuleInRootConfigBase
    {
        /// <summary>
        /// 这个列表是同种数据的列表。普通模块又托管的项，会在导出的时候加入这个列表。
        /// </summary>
        [XmlElement("托管模块汇总的托管项")]
        public List<TrusteeshipPackageBase> curTrusteeshipLst = new();


        /// <summary>
        /// 只会在导出xml的时候调用
        /// </summary>
        /// <param name="package"></param>
        /// <returns></returns>
        public int GenerationTrusteeshiId(TrusteeshipPackageBase package, ValueType moduleType)
        {
            if (package == null)
            {
                Debug.LogError($"托管的package为空  {this} ");
                return -1;
            }

            if (!curTrusteeshipLst.Contains(package))
            {
                curTrusteeshipLst.Add(package);
            }

            int index = curTrusteeshipLst.IndexOf(package);

            var TrusteeshipId = XmlConfigurableUtils.EncodeTrusteeshipId(moduleType, index);
            package.configId = TrusteeshipId;

            return TrusteeshipId;
        }

        Dictionary<int, TrusteeshipPackageBase> getCache = new();
        public TrusteeshipPackageBase GetTrusteeshipPackage(int TrusteeshipId)
        {
            if (getCache.ContainsKey(TrusteeshipId))
            {
                return getCache[TrusteeshipId];
            }
            foreach (TrusteeshipPackageBase item in curTrusteeshipLst)
            {
                if (item.configId == TrusteeshipId)
                {
                    return item;
                }
            }
            Debug.LogError($"GetTrusteeshipPackage => null   {TrusteeshipId} ");
            return null;
        }

    }


    public class TrusteeshipRecord
    {
        [XmlAttribute("托管的Ids_Guids")]
        public int[] commonTrusteeIds;
        [XmlAttribute("托管的Types_TypeIds")]
        public int[] commonTypeLst;
        [XmlAttribute("托管模块的TypeNames")]
        public string[] commonTypeNameLst;

        TrusteeVoucher curVoucher;
        public TrusteeVoucher CurVoucher => curVoucher;
        /// <summary>
        /// 生成票据
        /// </summary>
        /// <returns></returns>
        public virtual TrusteeVoucher GenerationVoucher(string moduleName, int configId)
        {
            if (curVoucher == null && commonTrusteeIds != null)
            {
                curVoucher = new TrusteeVoucher();
                curVoucher.moduleName = moduleName;
                curVoucher.configId = configId;

                XmlVoucherService.Agent.TrutreeshipRecordLoad(this, curVoucher);

                curVoucher.vouchers = new();
                for (int i = 0; i < commonTrusteeIds.Length; i++)
                {
                    var curTrusteesId = commonTrusteeIds[i];
                    var handler = XmlDefine.allModuleInRootHandlers[XmlRootType.Common][(XmlDefine.Common_ModuleType)commonTypeLst[i]];

                    var commonData = handler.GetData() as CommonTrusteeData;
                    var curPackage = commonData.GetTrusteeshipPackage(curTrusteesId);

                    foreach (TrusteeshipElementInPackageBase itemInPackage in (curPackage as ITrusteeshipDataOperation<TrusteeshipElementInPackageBase>).GetElements())
                    {
                        var voucherItem = new VoucherItem(itemInPackage);
                        curVoucher.vouchers.Add(voucherItem);
                    }
                }
            }
            return curVoucher;
        }

        /// <summary>
        /// 导出的时候调用
        /// </summary>
        /// <param name="lst"></param>
        public static TrusteeshipRecord GenerationRecord(IEnumerable<TrusteeshipPackageBase> lst)
        {
            var record = new TrusteeshipRecord();

            var ids = new List<int>();
            var names = new List<string>();
            var moduleTypes = new List<int>();

            foreach (var package in lst)
            {

                foreach (var commonHandlerMap in XmlDefine.allModuleInRootHandlers[XmlRootType.Common])
                {
                    var moduleType = commonHandlerMap.Key;
                    var moduleHandler = commonHandlerMap.Value;
                    var packageType = (moduleHandler as ITrusteeshipPackageType).GetTrusteeshipPackageType();
                    if (packageType == package.GetType())
                    {
                        var commonData = moduleHandler.GetData() as CommonTrusteeData;
                        var TrusteeId = commonData.GenerationTrusteeshiId(package, moduleType);
                        ids.Add(TrusteeId);
                        names.Add(moduleType.ToString());
                        moduleTypes.Add((int)(ValueType)moduleType);
                    }
                }

                record.commonTrusteeIds = ids.ToArray();
                record.commonTypeLst = moduleTypes.ToArray();
                record.commonTypeNameLst = names.ToArray();

            }
            return record;
        }
    }

    [XmlType("TrusteeVoucher")]
    public class TrusteeVoucher : ModuleInRootConfigBase
    {
        [XmlElement("托管选项")]
        public List<VoucherItem> vouchers = new();
    }


    public interface CommonTrusteeDataExternalParams
    {
        string[] GetExternalStringParams();
        float[] GetExternalFloatParams();
    }

    [XmlType("VoucherItem")]
    public class VoucherItem : ModuleInRootConfigBase
    {
        public VoucherItem() { }
        [XmlArray("字符串参数")]
        public string[] externalStrings;
        [XmlArray("浮点数参数")]
        public float[] externalFloats;
        public VoucherItem(TrusteeshipElementInPackageBase TrusteeData)
        {
            var externalData = TrusteeData as CommonTrusteeDataExternalParams;
            if (externalData != null)
            {
                externalStrings = externalData.GetExternalStringParams();
                externalFloats = externalData.GetExternalFloatParams();
            }
            moduleName = TrusteeData.moduleName;
            configId = TrusteeData.configId;
            active = TrusteeData.active;
        }
    }


    public interface ITrusteeshipDataOperation<out element>
        where element : TrusteeshipElementInPackageBase
    {
        IEnumerable<element> GetElements();
        element GetElementByConfigId(int configId);
    }

    public abstract class TrusteeshipPackageBase : ModuleInRootConfigBase
    {
    }

    public abstract class TrusteeshipPackageBase<element> : TrusteeshipPackageBase, ITrusteeshipDataOperation<element>
        where element : TrusteeshipElementInPackageBase, new()
    {
        public abstract IEnumerable<element> GetElements();
    

        public abstract void AddTrutrrshipElement(element element);


        Dictionary<int, element> elementCacheOnVocherRead = new();
        protected override void OnLoadComplete()
        {
            foreach (var element in GetElements())
            {
                elementCacheOnVocherRead.Add(element.configId, element);
            }
        }

        public element GetElementByConfigId(int configId)
        {
            if (elementCacheOnVocherRead.ContainsKey(configId))
            {
                return elementCacheOnVocherRead[configId];
            }
            Debug.LogError($"当前Package不包含Element{configId}");
            return null;
        }


        public static TrusteeshipPackageBase<element> GetTrusteeshipPackage<T>(params element[] elements) where T : TrusteeshipPackageBase<element>, new()
        {
            T ret = new();
            foreach (var element in elements)
            {
                ret.AddTrutrrshipElement(element);
            }
            return ret;
        }


        public static TrusteeshipPackageBase<element> GetTrusteeshipPackage<T>(ValueType moduleType, params ValueType[] elementEnums) where T : TrusteeshipPackageBase<element>, new()
        {
            T ret = new() { configId = (int)moduleType, moduleName = moduleType.ToString() };
            
            for (int i = 0; i < elementEnums.Length; i++)
            {
                element elementInstance = new();
                elementInstance.configId = (int)elementEnums[i];
                elementInstance.moduleName = elementEnums[i].ToString();
                ret.AddTrutrrshipElement(elementInstance);
            }
            return ret;
        }

    }

    public abstract class TrusteeshipElementInPackageBase : ModuleInRootConfigBase
    {

    }

}