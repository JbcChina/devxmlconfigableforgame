using System;
using System.Collections.Generic;
using System.IO;
using System.Xml.Serialization;

namespace XmlConfigable
{
    public sealed class NeedTrusteeshipModuleInRootConfig : ModuleInRootConfigBase, IXmlDataInitOnExportBefore
    {
        [XmlIgnore]
        public int ConfigID { get => configId; set => configId = value; }
        [XmlIgnore]
        public string ModuleName { get => moduleName; set => moduleName = value; }

        [XmlElement("托管存据")]
        public TrusteeshipRecord record;
        [XmlElement("托管凭据")]
        public TrusteeVoucher voucher;
        public NeedTrusteeshipModuleInRootConfig() { }
        public NeedTrusteeshipModuleInRootConfig(IEnumerable<TrusteeshipPackageBase> packageLst)
        {
            TrusteeshiPackageList = new(packageLst);
        }

        public static NeedTrusteeshipModuleInRootConfig GetNeedTrusteeshipConfig(params TrusteeshipPackageBase[] packageLst)
        {
            NeedTrusteeshipModuleInRootConfig ret = new(packageLst);
            //ret.moduleName = moduleType.ToString();
            //ret.configId = (int)(ValueType)moduleType;
            return ret;
        }


        /// <summary>
        /// 需要托管就在TrusteeshipPackageBase对象上打标签 XmlIndependentTrusteePackageAttribute
        /// </summary>
        [XmlIgnore]
        public List<TrusteeshipPackageBase> TrusteeshiPackageList = new();

        public void InitXmlExportData()
        {
            if (TrusteeshiPackageList!= null && TrusteeshiPackageList.Count != 0)
            {
                record = TrusteeshipRecord.GenerationRecord(TrusteeshiPackageList);
                voucher = record.GenerationVoucher(moduleName, configId);
            }
        }

    }

    public class NeedTrusteeshipModuleInRootHandler : Singleton<NeedTrusteeshipModuleInRootHandler>
    {
        public IXmlProxyExportAndLoad GetXmlIOProxy(NeedTrusteeshipModuleInRootConfig config)
        {
            var ret = Path.Combine(XmlDefine.ExportRootDirectory, $"/TrusteeshipModule");
            if (!Directory.Exists(ret))
            {
                Directory.CreateDirectory(ret);
            }
            return new ExportAndLoadProxy(ret, config.moduleName);
        }
    }




}