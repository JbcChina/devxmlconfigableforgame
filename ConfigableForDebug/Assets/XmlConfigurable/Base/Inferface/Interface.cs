using System;
using System.Collections.Generic;

namespace XmlConfigable
{

    public interface IXmlProxyExportAndLoad
    {
        void Export(object xmlConfig, Type[] extraTypes = null);
        object LoadXml(Type xmlType);
    }

    public interface IXmlDataLoadLife
    {
        void LoadXml(XmlConfigBase xmlConfig);
        void OnXmlLoadCompleted();
    }

    public interface IXmlDataInitOnExportBefore
    {
        void InitXmlExportData();
    }

    public interface FitExternalTrutershipModule
    {
        IEnumerable<ValueType> GetExternalTrusteeshipModulesType();
    }


    public interface IXmlHandlerInstaceLife
    {
        void InstaceComplete();
    }

    public interface ISetHandlerType
    {
        void SetRootType(XmlRootType type);
    }

    public interface IRootHandlerLife : ISetHandlerType
    {
        XmlRootType GetRootType();
    }

    public interface IModuleInRootHandlerLife : ISetHandlerType
    {
        XmlRootType GetRootType();
    }

    public interface IXmlHandlerWithData<out Data> 
        where Data : XmlConfigBase
    {
        public Type GetDataType()
        {
            return typeof(Data);
        }
        Data GetData();
        Data GetExportData(bool getNew);
    }


    public interface IXmlRootHandler<out Data> : IXmlHandlerWithData<Data>, IRootHandlerLife
        where Data : RootConfigBase
    {

    }

    public interface IXmlModuleInRootHandler<out Data> : IXmlHandlerWithData<Data>, IModuleInRootHandlerLife
        where Data : ModuleInRootConfigBase
    {
        public void SetModuleTypeInRoot(Enum moduleType);
    }


}