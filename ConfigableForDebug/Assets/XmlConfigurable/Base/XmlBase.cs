using System;
using System.Collections.Generic;
using System.Xml.Serialization;
using UnityEngine;

namespace XmlConfigable
{
    public class XmlConfigBase
    {
        //[XmlIgnore]
        //public IXmlProxyExportAndLoad ioProxy { get; set; }

        [XmlElement("持有的票据")]
        public List<NeedTrusteeshipModuleInRootConfig> holdingVouchers;

        [XmlElement("外部凭据")]
        public List<TrusteeVoucher> externalVouchers;

        public void XmlConfigLoadComplete()
        {
            if (holdingVouchers != null)
            {
                foreach (NeedTrusteeshipModuleInRootConfig config in holdingVouchers)
                {
                    XmlVoucherService.Agent.TrutreeshipRecordLoad(config.record, config.voucher);
                }
            }
            if (externalVouchers != null)
            {
                foreach (var item in externalVouchers)
                {
                    XmlVoucherService.Agent.VocherRecord(item);
                }
            }
            OnLoadComplete();
        }
        protected virtual void OnLoadComplete()
        {}


    }

    /// <summary>
    /// 导出的时候获取，持有外部托管模块的类型。会在导出的时候，自动添加到“外部凭据”
    /// </summary>
    public interface IHoldingExternalTrusteeModule
    {
        IEnumerable<Enum> GetTrusteeModuleTypes();
    }

    public interface IXmlHandlerIOProxy
    {
        public IXmlProxyExportAndLoad GetXmlIOProxy();
    }

    public abstract class XmlHandlerBase<handler, config> : Singleton<handler>, IInit, ISetHandlerType,IXmlHandlerIOProxy, IXmlDataLoadLife
        where handler : class, new()
        where config : XmlConfigBase, new()
    {
        public void Init()
        {
            (this as IXmlHandlerInstaceLife)?.InstaceComplete();
        }
        protected XmlRootType xmlRootType { get; private set; }
        public void SetRootType(XmlRootType type)
        {
            xmlRootType = type;
        }
        public XmlRootType GetRootType()
        {
            return xmlRootType;
        }

        private config dataConfig;
        public config CurData => GetData();
        public config GetData()
        {
            if (dataConfig == null)
            {
                Debug.LogError("配置数据是null，xml还没有Load。当前数据依赖xml的导入。");
            }
            return dataConfig;
        }

        public config GetExportData(bool getNew)
        {
            if (getNew)
            {
                config data = new();
                dataConfig = data;
                OnExportDataInstance(data);
            }
            return dataConfig;
        }
        /// <summary>
        /// 导出xml的时候，new出来的xml数据对象
        /// </summary>
        /// <param name="curCreateData"></param>
        public virtual void OnExportDataInstance(config curCreateData) { }


        public abstract IXmlProxyExportAndLoad GetXmlIOProxy();

        public void LoadXml(XmlConfigBase xmlConfig)
        {
            dataConfig = xmlConfig as config;
            if (dataConfig == null)
            {
                Debug.LogError($"类型对不上： cur:{typeof(config)}   param:{xmlConfig?.GetType()}");
                return;
            }
            OnXmlLoad(dataConfig);
            OnXmlLoadCompleted();
        }

        public virtual void OnXmlLoad(config data)
        {
        }
        public virtual void OnXmlLoadCompleted()
        {
        }
    }
}