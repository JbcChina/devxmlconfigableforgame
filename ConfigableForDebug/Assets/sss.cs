using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class sss : MonoBehaviour
{
    static bool printStart = false;
    static bool printUpdate = false;
    static bool printFixedUpdate = false;
    static bool printLateUpdate = false;
    static int sign;
    public static void StartPrint(int _sign)
    {
        Debug.LogError($"StartPrint  SetValueForward    {_sign}        ____________________________________________________________________________");
        printStart = true;
        printUpdate = true;
        printFixedUpdate = true;
        printLateUpdate = true;
        sign = _sign;
        Debug.LogError($"StartPrint  SetValueBack  {sign}        ____________________________________________________________________________");
    }

    void Start()
    {
        if (printStart)
        {
            Debug.LogError($"Start    {sign}\t\tcurFrameCount:{Time.frameCount}");
            printStart = false;
        }
    }

    void Update()
    {

        if (printUpdate)
        {
            Debug.LogError($"Update    {sign}\t\tcurFrameCount:{Time.frameCount}");
            printUpdate = false;
        }
    }

    private void FixedUpdate()
    {

        if (printFixedUpdate)
        {
            Debug.LogError($"FixedUpdate    {sign}\t\tcurFrameCount:{Time.frameCount}");
            printFixedUpdate = false;
        }
    }
    private void LateUpdate()
    {
        if (printLateUpdate)
        {
            Debug.LogError($"LateUpdate    {sign}\t\tcurFrameCount:{Time.frameCount}");
            printLateUpdate = false;
        }
    }
}
