
using System;

public interface IInit
{
    void Init();
}

public class Singleton<T> where T : class, new()
{
    private static T _Instance;

    static Singleton()
    {
        Singleton<T>._Instance = new T();
    }

    public static void CreateInstance()
    {
        if (Singleton<T>._Instance == null)
        {
            Singleton<T>._Instance = Activator.CreateInstance<T>();
            (Singleton<T>._Instance as IInit)?.Init();
        }
    }

    public static void DestroyInstance()
    {
        if (Singleton<T>._Instance != null)
        {
            Singleton<T>._Instance = null;
        }
    }

    public static T Instance
    {
        get
        {
            if (Singleton<T>._Instance == null)
            {
                CreateInstance();
            }
            return Singleton<T>._Instance;
        }
    }
}
